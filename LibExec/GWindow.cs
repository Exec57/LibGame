﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibExec
{
    public abstract class GWindow : Game
    {
        GraphicsDeviceManager graphics;
        static Form form;
        
        MethodInfo startMethod;
        MethodInfo updateMethod;
        MethodInfo drawMethod;

        public string[] args { get; internal set; }

        internal static SpriteBatch spriteBatch { get; private set; }

        internal static Scene scene;

        public GWindow()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            Screen.SetSize(800, 600);
            IsMouseVisible = true;
            graphics.PreferredBackBufferWidth = Screen.width;
            graphics.PreferredBackBufferHeight = Screen.height;
            Window.AllowUserResizing = true;
            form = (Form)Form.FromHandle(Window.Handle);
            form.ClientSizeChanged += Form_ClientSizeChanged;
            form.FormClosing += Form_FormClosing;

            startMethod = GetType().GetMethod("Start", BindingFlags.Instance | BindingFlags.NonPublic);
            updateMethod = GetType().GetMethod("Update", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            drawMethod = GetType().GetMethod("Draw", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly);
        }

        private void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            Network.Stop();
        }

        private void Form_ClientSizeChanged(object sender, EventArgs e)
        {
            Screen.SetSize(Window.ClientBounds.Width, Window.ClientBounds.Height);
            graphics.PreferredBackBufferWidth = Screen.width;
            graphics.PreferredBackBufferHeight = Screen.height;
            graphics.ApplyChanges();
        }

        protected sealed override void Initialize()
        {
            Vector2 screenSize = new Vector2(GraphicsDevice.DisplayMode.Width, GraphicsDevice.DisplayMode.Height);

            Point winPos = (screenSize / 2 - Screen.center).ToPoint();
            form.SetDesktopLocation(winPos.X, winPos.Y);

            base.Initialize();
        }

        protected sealed override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Resources.Init(Content, GraphicsDevice);

            CheckArgs();

            if (startMethod != null)
                startMethod.Invoke(this, null);
        }

        protected sealed override void UnloadContent()
        {

        }

        protected sealed override void Update(GameTime gameTime)
        {
            Time.Update(gameTime);

            if (IsActive)
                Input.BeginUpdate();

            Network.Update();

            if (updateMethod != null)
                updateMethod.Invoke(this, null);

            if (scene != null)
                scene.DoUpdate();

            base.Update(gameTime);

            if (IsActive)
                Input.EndUpdate();
        }

        protected sealed override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            if (drawMethod != null)
                drawMethod.Invoke(this, null);

            if (scene != null)
                scene.DoDraw();

            base.Draw(gameTime);
        }

        public static void ExitGame()
        {
            form.Close();
        }

        void CheckArgs()
        {
            try
            {
                for (int i = 0; i < args.Length; i++)
                {
                    if (args[i] == "-x")
                        form.SetDesktopLocation(Convert.ToInt32(args[i + 1]), form.DesktopLocation.Y);

                    if (args[i] == "-listen")
                        Network.ListenServer(7777);

                    if (args[i] == "-connect")
                        Network.Connect(Convert.ToString(args[i + 1]), 7777);
                }
            }
            catch { }  
        }
    }
}
