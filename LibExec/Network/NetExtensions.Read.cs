﻿using Lidgren.Network;
using Microsoft.Xna.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LibExec
{
    internal static partial class NetExtensions
    {
        public static Behaviour ReadBehaviour(this NetBuffer buffer)
        {
            int id = buffer.ReadInt32();
            if (id == 0)
                return Scene.current;

            if (id == -1)
                return null;

            if (Scene.networkActors.ContainsKey(id))
                return Scene.networkActors[id];
            return null;
        }

        public static Actor ReadActor(this NetBuffer buffer)
        {
            return (Actor)buffer.ReadBehaviour();
        }

        public static Scene ReadNewScene(this NetBuffer buffer)
        {
            Type type = buffer.ReadType();
            return (Scene)Activator.CreateInstance(type);
        }

        public static Actor ReadNewActor(this NetBuffer buffer)
        {
            Type type = buffer.ReadType();
            Actor instance = (Actor)Activator.CreateInstance(type);
            instance.id = buffer.ReadInt32();
            instance.owner = buffer.ReadActor();
            instance.position = buffer.ReadVector2();
            instance.rotation = buffer.ReadFloat();
            Scene.networkActors.Add(instance.id, instance);
            if (instance.bUpdatable)
                Scene.updatableActors.Add(instance);
            if (instance.bDrawable)
                Scene.drawableActors.Add(instance.layer, instance);
            return instance;
        }

        public static IList ReadList(this NetBuffer buffer, Type type)
        {
            IList list = (IList)Activator.CreateInstance(type);
            Type objType = type.GetGenericArguments().Single();

            int count = buffer.ReadInt32();

            for (int i = 0; i < count; i++)
                list.Add(buffer.ReadFromType(objType));

            return list;
        }

        public static Type ReadType(this NetBuffer buffer)
        {
            byte index = buffer.ReadByte();
            return Network.types.ElementAt(index).Key;
        }

        public static RPCInfo ReadRPC(this NetBuffer buffer)
        {
            Behaviour instance = buffer.ReadBehaviour();
            if (instance == null)
                return null;
            return instance.methods.ElementAt(buffer.ReadByte()).Value;
        }

        public static SyncInfo ReadSyncInfo(this NetBuffer buffer, Behaviour instance)
        {
            byte index = buffer.ReadByte();
            return instance.syncList[index];
        }

        public static NetPlayer ReadNetPlayer(this NetBuffer buffer)
        {
            return Network.netPlayers[buffer.ReadByte()];
        }

        public static object[] ReadArgs(this NetBuffer buffer, MethodInfo method)
        {
            byte count = buffer.ReadByte();
            object[] args = new object[count];

            for (int i = 0; i < count; i++)
            {
                Type type = method.GetParameters()[i].ParameterType;
                args[i] = buffer.ReadFromType(type);
            }
            return args;
        }

        public static object ReadStruct(this NetBuffer buffer, Type type)
        {
            object instance = Activator.CreateInstance(type);
            FieldInfo[] info = type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            foreach (var item in info)
                item.SetValue(instance, buffer.ReadFromType(item.FieldType));
            return instance;
        }

        public static object[] ReadArray(this NetBuffer buffer)
        {
            byte count = buffer.ReadByte();
            Type type = buffer.ReadType();
            object[] objs = (object[])Array.CreateInstance(type, count);
            for (int i = 0; i < count; i++)
                objs[i] = buffer.ReadFromType(type);
            return objs;
        }

        public static object ReadFromType(this NetBuffer buffer, Type type)
        {
            if (type == typeof(byte))
                return buffer.ReadByte();
            else if (type == typeof(bool))
                return buffer.ReadBoolean();
            else if (type == typeof(int))
                return buffer.ReadInt32();
            else if (type == typeof(string))
                return buffer.ReadString();
            else if (type == typeof(float))
                return buffer.ReadFloat();
            else if (type == typeof(Vector2))
                return buffer.ReadVector2();
            else if (type == typeof(Point))
                return buffer.ReadPoint();
            else if (typeof(Actor).IsAssignableFrom(type))
                return buffer.ReadActor();
            else if (type.IsArray)
                return buffer.ReadArray();
            else if (typeof(IList).IsAssignableFrom(type))
                return buffer.ReadList(type);
            else if (type == typeof(NetPlayer))
                return buffer.ReadNetPlayer();
            else if (type.IsValueType && !type.IsEnum && !type.IsPrimitive)
                return buffer.ReadStruct(type);
            return null;
        }
    }
}
