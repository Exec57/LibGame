﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;
using Microsoft.Xna.Framework;

namespace LibExec
{
    internal enum NetworkData
    {
        Login = 0,
        PostLogin = 1,
        NewActor = 2,
        NewActorWithAuthority = 3,
        Sync = 4,
        SyncWithNoHookEvent = 5,
        SyncPosition = 6,
        SyncRotation = 7,
        Destroy = 8,
        RPC = 9
    }

    public static partial class Network
    {
        internal static NetServer server { get; private set; }
        static NetPeerConfiguration config;
        internal static NetClient client { get; private set; }

        static string appIdentifier = "LibExec";

        internal static List<NetConnection> connections = new List<NetConnection>();

        internal static List<NetPlayer> netPlayers = new List<NetPlayer>();

        public static NetPlayer netPlayer { get; internal set; }
        public static Actor localPlayer { get; internal set; }

        internal static Dictionary<Type, byte> types { get; private set; }

        internal static bool bIsListenServer { get; private set; }

        static NetIncomingMessage inc;

        static void Init()
        {
            config = new NetPeerConfiguration(appIdentifier);

            config.DisableMessageType(NetIncomingMessageType.ConnectionLatencyUpdated);
            config.DisableMessageType(NetIncomingMessageType.DebugMessage);
            config.DisableMessageType(NetIncomingMessageType.DiscoveryRequest);
            config.DisableMessageType(NetIncomingMessageType.DiscoveryResponse);
            config.DisableMessageType(NetIncomingMessageType.Error);
            config.DisableMessageType(NetIncomingMessageType.ErrorMessage);
            config.DisableMessageType(NetIncomingMessageType.NatIntroductionSuccess);
            config.DisableMessageType(NetIncomingMessageType.Receipt);
            config.DisableMessageType(NetIncomingMessageType.UnconnectedData);
            config.DisableMessageType(NetIncomingMessageType.VerboseDebugMessage);
            config.DisableMessageType(NetIncomingMessageType.WarningMessage);

            types = new Dictionary<Type, byte>();
            Type[] array = Assembly.GetEntryAssembly().GetTypes();
            for (byte i = 0; i < array.Length; i++)
                types.Add(array[i], i);
        }

        public static bool ListenServer(int port)
        {
            bool state = Host(port);
            if (state)
                bIsListenServer = true;
            return state;
        }

        public static bool Host(int port)
        {
            try
            {
                Init();

                config.Port = port;
                config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);

                server = new NetServer(config);
                server.Start();

                return true;
            }
            catch
            {
                server = null;
                types = null;
                config = null;
            }

            return false;
        }

        public static void Connect(string ip, int port)
        {
            Init();

            client = new NetClient(config);
            client.Start();
            client.Connect(ip, port);
        }

        internal static void Update()
        {
            if (IsServer())
                UpdateServer();
            else if (IsClient())
                UpdateClient();
        }

        public static void Stop()
        {
            if (client != null)
            {
                client.Disconnect("");
                client.Shutdown("");
            }
            if (server != null)
                server.Shutdown("");
        }

        public static bool IsServer()
        {
            return server != null && server.Status == NetPeerStatus.Running;
        }

        public static bool IsClient()
        {
            return client != null && client.Status == NetPeerStatus.Running;
        }

        public static bool IsListenServer()
        {
            return IsServer() && bIsListenServer;
        }

        public static bool IsServerOnly()
        {
            return IsServer() && Target.bServerOnly;
        }

        public static bool IsValid()
        {
            return IsServer() || IsClient();
        }

        internal static NetOutgoingMessage CreateMessage(NetworkData data)
        {
            NetOutgoingMessage outmsg;
            if (IsServer())
                outmsg = server.CreateMessage();
            else
                outmsg = client.CreateMessage();
            outmsg.Write((byte)data);
            return outmsg;
        }

        internal static NetPlayer GetNetPlayer(NetConnection connection)
        {
            foreach (var item in netPlayers)
            {
                if (item.connection == connection)
                    return item;
            }
            return null;
        }

        static void RPC()
        {
            RPCInfo rpc = inc.ReadRPC();

            if (rpc == null)
                return;

            object[] args = inc.ReadArgs(rpc.method);

            rpc.Invoke(args);
        }
    }
}
