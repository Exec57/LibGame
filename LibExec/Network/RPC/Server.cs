﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;

namespace LibExec
{
    public sealed class Server : Attribute
    {
        public readonly Delivery delivery;

        public Server()
        {
            delivery = Delivery.ReliableSequenced;
        }

        public Server(Delivery delivery)
        {
            this.delivery = delivery;
        }

        internal static void RPC(RPCInfo rpc, params object[] args)
        {
            if (Network.IsServer())
                rpc.Invoke(args);
            else
            {
                NetOutgoingMessage outmsg= Network.CreateMessage(NetworkData.RPC);
                outmsg.Write(rpc);
                outmsg.Write(args);
                Network.client.SendMessage(outmsg, rpc.netDeliveryMethod);
            }
        }
    }
}
