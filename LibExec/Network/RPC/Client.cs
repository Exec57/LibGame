﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;

namespace LibExec
{
    public sealed class Client : Attribute
    {
        public readonly Delivery delivery;

        public Client()
        {
            delivery = Delivery.ReliableSequenced;
        }

        public Client(Delivery delivery)
        {
            this.delivery = delivery;
        }

        internal static void RPC(RPCInfo rpc, params object[] args)
        {
            if (!Network.IsServer())
                rpc.Invoke(args);
            else
            {
                if (rpc.instance.ownerConnection == null)
                {
                    rpc.Invoke(args);
                    return;
                }

                NetOutgoingMessage outmsg = Network.CreateMessage(NetworkData.RPC);
                outmsg.Write(rpc);
                outmsg.Write(args);
                Network.server.SendMessage(outmsg, rpc.instance.ownerConnection, rpc.netDeliveryMethod);
            }
        }
    }
}
