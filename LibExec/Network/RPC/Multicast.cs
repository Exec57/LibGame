﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;

namespace LibExec
{
    public sealed class Multicast : Attribute
    {
        public readonly Delivery delivery;

        public Multicast()
        {
            delivery = Delivery.ReliableSequenced;
        }

        public Multicast(Delivery delivery)
        {
            this.delivery = delivery;
        }

        internal static void RPC(RPCInfo rpc, params object[] args)
        {
            if (!Network.IsServer())
                rpc.Invoke(args);
            else
            {
                NetOutgoingMessage outmsg = Network.CreateMessage(NetworkData.RPC);
                outmsg.Write(rpc);
                outmsg.Write(args);
                Network.server.SendToAll(outmsg, rpc.netDeliveryMethod);

                rpc.Invoke(args);
            }
        }
    }
}
