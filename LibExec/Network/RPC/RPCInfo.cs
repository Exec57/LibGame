﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LibExec
{
    public enum Delivery
    {
        ReliableSequenced,
        Reliable,
        Unreliable
    }

    internal sealed class RPCInfo
    {
        public readonly Behaviour instance;
        public readonly MethodInfo method;
        public readonly byte index;
        public NetDeliveryMethod netDeliveryMethod { get; private set; }
        public Server server { get; private set; }
        public Client client { get; private set; }
        public Multicast multicast { get; private set; }

        public RPCInfo(Behaviour instance, MethodInfo method, byte index)
        {
            this.instance = instance;
            this.method = method;
            this.index = index;
        }

        public void SetServer(Server server)
        {
            this.server = server;
            UpdateDelivery(server.delivery);
        }

        public void SetClient(Client client)
        {
            this.client = client;
            UpdateDelivery(client.delivery);
        }

        public void SetMulticast(Multicast multicast)
        {
            this.multicast = multicast;
            UpdateDelivery(multicast.delivery);
        }

        void UpdateDelivery(Delivery delivery)
        {
            if (delivery == Delivery.Reliable)
                netDeliveryMethod = NetDeliveryMethod.ReliableOrdered;
            else if (delivery == Delivery.ReliableSequenced)
                netDeliveryMethod = NetDeliveryMethod.ReliableSequenced;
            else if (delivery == Delivery.Unreliable)
                netDeliveryMethod = NetDeliveryMethod.UnreliableSequenced;
        }

        public void Invoke(object[] args)
        {
            if (method != null)
                method.Invoke(instance, args);
        }

        public static Dictionary<string, RPCInfo> CreateDictionary(Behaviour instance)
        {
            Dictionary<string, RPCInfo> dico = new Dictionary<string, RPCInfo>();
            MethodInfo[] info = instance.GetType().GetMethods(BindingFlags.Instance | BindingFlags.NonPublic);
            foreach (var method in info)
            {
                RPCInfo rpc = new RPCInfo(instance, method, (byte)dico.Count);

                Server server;
                Client client;
                Multicast multicast;

                if ((server = method.GetCustomAttribute<Server>()) != null)
                    rpc.SetServer(server);
                else if ((client = method.GetCustomAttribute<Client>()) != null)
                    rpc.SetClient(client);
                else if ((multicast = method.GetCustomAttribute<Multicast>()) != null)
                    rpc.SetMulticast(multicast);
                else
                    continue;

                dico.Add(method.Name, rpc);
            }

            return dico;
        }
    }
}
