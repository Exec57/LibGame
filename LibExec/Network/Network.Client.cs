﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LibExec
{
    public static partial class Network
    {
        static void UpdateClient()
        {
            while ((inc = Network.client.ReadMessage()) != null)
            {
                switch (inc.MessageType)
                {
                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)inc.ReadByte();
                        if (status == NetConnectionStatus.Disconnected)
                            Scene.Reset();
                        break;
                    case NetIncomingMessageType.Data:
                        NetworkData data = (NetworkData)inc.ReadByte();
                        if (data == NetworkData.Login)
                            ClientLogin();
                        else if (data == NetworkData.NewActor)
                            NewActor();
                        else if (data == NetworkData.NewActorWithAuthority)
                            NewActorWithAuthority();
                        else if (data == NetworkData.Sync)
                            ClientSync(true);
                        else if (data == NetworkData.SyncWithNoHookEvent)
                            ClientSync(false);
                        else if (data == NetworkData.SyncPosition)
                            ClientSyncPosition();
                        else if (data == NetworkData.SyncRotation)
                            ClientSyncRotation();
                        else if (data == NetworkData.Destroy)
                            Destroy();
                        else if (data == NetworkData.RPC)
                            RPC();
                        break;
                }
            }

            Network.client.Recycle(inc);
        }

        static void ClientLogin()
        {
            netPlayer = new NetPlayer(client.Connections[0], inc.ReadByte());

            Scene scene = inc.ReadNewScene();
            Scene.Load<Scene>(scene);

            short count = inc.ReadInt16();
            for (int i = 0; i < count; i++)
                NewActor();

            scene.DoStart();

            NetOutgoingMessage outmsg = Network.CreateMessage(NetworkData.PostLogin);
            Network.client.SendMessage(outmsg, NetDeliveryMethod.ReliableOrdered);
        }

        static void NewActor()
        {
            Actor instance = inc.ReadNewActor();
            instance.DoStart();
        }

        static void NewActorWithAuthority()
        {
            Actor instance = inc.ReadNewActor();
            long id = inc.ReadInt64();
            if (id == client.UniqueIdentifier)
            {
                instance.bIsLocalPlayer = true;
                Network.localPlayer = instance;
            }
            instance.DoStart();
        }

        static void ClientSync(bool bHookEvent)
        {
            Behaviour instance = inc.ReadBehaviour();
            SyncInfo info = inc.ReadSyncInfo(instance);
            Type syncType = info.GetFieldType();
            info.SetValue(inc.ReadFromType(syncType), bHookEvent);
        }

        static void ClientSyncPosition()
        {
            Actor instance = inc.ReadActor();
            if (instance == null)
                return;
            instance.targetPosition = inc.ReadVector2();
        }

        static void ClientSyncRotation()
        {
            Actor instance = inc.ReadActor();
            if (instance == null)
                return;
            instance.targetRotation = inc.ReadFloat();
        }

        static void Destroy()
        {
            Actor instance = inc.ReadActor();
            instance.bPendingKill = true;
            if (instance.bReplicate)
                Scene.networkActors.Remove(instance.id);
        }
    }
}
