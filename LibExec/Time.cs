﻿using Microsoft.Xna.Framework;

namespace LibExec
{
    public static class Time
    {
        public static GameTime gameTime { get; private set; }
        public static float deltaTime { get; private set; }
        public static float time { get; private set; }

        internal static void Update(GameTime gameTime)
        {
            Time.gameTime = gameTime;
            deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            time = (float)gameTime.TotalGameTime.TotalSeconds;
        }
    }
}
