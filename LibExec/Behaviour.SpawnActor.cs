﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;

namespace LibExec
{
    public abstract partial class Behaviour
    {
        internal T InternalSpawnActor<T>() where T : Actor
        {
            Actor instance = (Actor)Activator.CreateInstance(typeof(T));

            if (Network.IsServer() && instance.bReplicate)
            {
                instance.id = Scene.networkCountID++;
                instance.ownerConnection = ownerConnection;
                Scene.networkActors.Add(instance.id, instance);
            }
            else
            {
                instance.id = Scene.countID++;
                Scene.actors.Add(instance.id, instance);
            }
            if (instance.bUpdatable)
                Scene.updatableActors.Add(instance);
            if (instance.bDrawable)
                Scene.drawableActors.Add(instance.layer, instance);
            return (T)instance;
        }

        #region SpawnActor

        internal void ProcessSpawnActor(Actor instance)
        {
            if (Network.IsServer() && instance.bReplicate)
            {
                NetOutgoingMessage outmsg = Network.CreateMessage(NetworkData.NewActor);
                outmsg.WriteNewActor(instance);
                Network.server.SendToAll(outmsg, NetDeliveryMethod.ReliableOrdered);
            }

            instance.DoStart();
        }

        protected T SpawnActor<T>(Actor owner = null) where T : Actor
        {
            T instance = InternalSpawnActor<T>();
            instance.owner = owner;
            ProcessSpawnActor(instance);
            return instance;
        }

        protected T SpawnActor<T>(Vector2 positon) where T : Actor
        {
            T instance = InternalSpawnActor<T>();
            instance.position = positon;
            ProcessSpawnActor(instance);
            return instance;
        }

        protected T SpawnActor<T>(Vector2 positon, float rotation, Actor owner = null) where T : Actor
        {
            T instance = InternalSpawnActor<T>();
            instance.position = positon;
            instance.rotation = rotation;
            instance.owner = owner;
            ProcessSpawnActor(instance);
            return instance;
        }

        #endregion

        #region SpawnActorWithAuthority

        internal void ProcessSpawnActorWithAuthority(Actor instance)
        {
            NetOutgoingMessage outmsg = Network.CreateMessage(NetworkData.NewActorWithAuthority);
            outmsg.WriteNewActor(instance);
            outmsg.Write(instance.ownerConnection.RemoteUniqueIdentifier);
            Network.server.SendToAll(outmsg, NetDeliveryMethod.ReliableOrdered);

            instance.DoStart();
        }

        protected T SpawnActorWithAuthority<T>(NetPlayer netPlayer, Actor owner = null) where T : Actor
        {
            if (!Network.IsServer())
                return null;

            T instance = InternalSpawnActor<T>();
            instance.ownerConnection = netPlayer.connection;
            instance.owner = owner;
            ProcessSpawnActorWithAuthority(instance);
            return instance;
        }

        protected T SpawnActorWithAuthority<T>(NetPlayer netPlayer, Vector2 positon) where T : Actor
        {
            if (!Network.IsServer())
                return null;

            T instance = InternalSpawnActor<T>();
            instance.ownerConnection = netPlayer.connection;
            instance.position = positon;
            ProcessSpawnActorWithAuthority(instance);
            return instance;
        }

        protected T SpawnActorWithAuthority<T>(NetPlayer netPlayer, Vector2 positon, float rotation, Actor owner = null) where T : Actor
        {
            if (!Network.IsServer())
                return null;

            T instance = InternalSpawnActor<T>();
            instance.ownerConnection = netPlayer.connection;
            instance.position = positon;
            instance.rotation = rotation;
            instance.owner = owner;
            ProcessSpawnActorWithAuthority(instance);
            return instance;
        }

        #endregion

        #region SpawnActorWithServerAuthority

        internal void ProcessSpawnActorWithServerAuthority(Actor instance)
        {
            instance.bIsLocalPlayer = true;
            Network.localPlayer = instance;
            instance.DoStart();
        }

        protected T SpawnActorWithServerAuthority<T>(Actor owner = null) where T : Actor
        {
            if (!Network.IsServer())
                return null;

            T instance = InternalSpawnActor<T>();
            instance.owner = owner;
            ProcessSpawnActorWithServerAuthority(instance);
            return instance;
        }

        protected T SpawnActorWithServerAuthority<T>(Vector2 position) where T : Actor
        {
            if (!Network.IsServer())
                return null;

            T instance = InternalSpawnActor<T>();
            instance.position = position;
            ProcessSpawnActorWithServerAuthority(instance);
            return instance;
        }

        protected T SpawnActorWithServerAuthority<T>(Vector2 position, float rotation, Actor owner = null) where T : Actor
        {
            if (!Network.IsServer())
                return null;

            T instance = InternalSpawnActor<T>();
            instance.position = position;
            instance.rotation = rotation;
            instance.owner = owner;
            ProcessSpawnActorWithServerAuthority(instance);
            return instance;
        }

        #endregion
    }
}
