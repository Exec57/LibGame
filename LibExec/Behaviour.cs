﻿using Lidgren.Network;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LibExec
{
    public abstract partial class Behaviour
    {
        internal readonly MethodInfo startMethod;
        internal readonly MethodInfo updateMethod;
        internal readonly MethodInfo drawMethod;
        internal readonly MethodInfo draw2DMethod;

        protected SpriteBatch spriteBatch { get { return GWindow.spriteBatch; } }

        public int sequenceChannel { get; protected set; }
        public bool bReplicate { get; protected set; }

        internal List<SyncInfo> syncList;
        internal Dictionary<string, RPCInfo> methods;

        protected int sendRate;
        internal float sendRateMs { get; private set; }

        internal NetConnection ownerConnection;
        internal int id;

        internal Actor actorInstance { get; private set; }

        public Behaviour()
        {
            startMethod = GetType().GetMethod("Start", BindingFlags.Instance | BindingFlags.NonPublic);
            updateMethod = GetType().GetMethod("Update", BindingFlags.Instance | BindingFlags.NonPublic);
            drawMethod = GetType().GetMethod("Draw", BindingFlags.NonPublic | BindingFlags.Instance);
            draw2DMethod = GetType().GetMethod("Draw2D", BindingFlags.Instance | BindingFlags.NonPublic);

            sendRate = 10;
        }

        internal virtual void DoStart()
        {
            actorInstance = this as Actor;

            if (bReplicate)
            {
                sendRateMs = 1.0f / sendRate;
                syncList = Sync.CreateList(this);
                methods = RPCInfo.CreateDictionary(this);
                
                if (Network.IsServer() && syncList.Count > 0)
                    Timer.Set(OnSync, this, sendRateMs, true);
            }
        }

        internal virtual void DoUpdate() { }

        internal virtual void DoDraw(){ }

        internal virtual void DoDraw2D() { }

        void OnSync()
        {
            Sync.Update(this);
        }

        public void RPC(string name, params object[] args)
        {
            if (methods == null)
                return;

            RPCInfo rpc;
            if (!methods.TryGetValue(name, out rpc))
                return;

            if (!Network.IsValid())
            {
                rpc.Invoke(args);
                return;
            }

            if (!bReplicate || !Check(this) || !HasRPCAuthority())
                return;

            if (rpc.server != null)
                Server.RPC(rpc, args);
            else if (rpc.client != null)
                Client.RPC(rpc, args);
            else if (rpc.multicast != null)
                Multicast.RPC(rpc, args);
        }

        internal bool HasRPCAuthority()
        {
            if (Network.IsServer())
                return true;

            if (actorInstance != null && actorInstance.bLocalPlayer && Network.localPlayer != this)
                return false;

            return true;
        }

        protected bool Check(Behaviour instance)
        {
            return Scene.Check(instance);
        }

        #region FindActor

        public static T FindActorOfType<T>() where T : Actor
        {
            foreach (var item in Scene.actors)
            {
                if (item.Value.GetType() == typeof(T))
                    return (T)item.Value;
            }

            foreach (var item in Scene.networkActors)
            {
                if (item.Value.GetType() == typeof(T))
                    return (T)item.Value;
            }

            return null;
        }

        public static T[] FindActorsOfType<T>() where T : Actor
        {
            List<T> actors = new List<T>();

            foreach (var item in Scene.actors)
            {
                if (item.Value.GetType() == typeof(T))
                    actors.Add((T)item.Value);
            }

            foreach (var item in Scene.networkActors)
            {
                if (item.Value.GetType() == typeof(T))
                    actors.Add((T)item.Value);
            }

            return actors.ToArray();
        }

        #endregion
    }
}
