﻿using Lidgren.Network;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LibExec
{
    public abstract class Scene : Behaviour
    {
        internal static Scene current;

        internal static Dictionary<int, Actor> actors { get; private set; }
        internal static SortedDictionary<int, Actor> networkActors { get; private set; }
        internal static List<Actor> updatableActors { get; private set; }
        internal static SortedList<int, Actor> drawableActors;

        internal static MethodInfo onPostLoginMethod { get; private set; }
        static MethodInfo onDisconnectedMethod;

        internal static int countID = 1;
        internal static short networkCountID = 1;

        public Scene()
        {
            current = this;

            Timer.timerList.Clear();
            actors = new Dictionary<int, Actor>();
            networkActors = new SortedDictionary<int, Actor>();
            updatableActors = new List<Actor>();
            drawableActors = new SortedList<int, Actor>(new DuplicateKeyComparer<int>());

            onPostLoginMethod = GetType().GetMethod("OnPostLogin", BindingFlags.Instance | BindingFlags.NonPublic);
            onDisconnectedMethod = GetType().GetMethod("OnDisconnected", BindingFlags.Instance | BindingFlags.NonPublic);
            sequenceChannel = 1;
        }

        internal override void DoStart()
        {
            base.DoStart();

            if (startMethod != null)
                startMethod.Invoke(this, null);
        }

        internal override void DoUpdate()
        {
            base.DoUpdate();

            Timer.Update();

            if (updateMethod != null)
                updateMethod.Invoke(this, null);

            for (int i = 0; i < updatableActors.Count; i++)
            {
                if (updatableActors[i].bPendingKill || updatableActors[i].bPendingKillUpdate)
                {
                    updatableActors[i].bPendingKillUpdate = false;
                    updatableActors.RemoveAt(i);
                    i--;
                }
                else if (updatableActors[i].bActive)
                    updatableActors[i].DoUpdate();
            }
        }

        internal override void DoDraw()
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone,
                null, Screen.GetTransform());

            base.DoDraw();

            if (drawMethod != null)
                drawMethod.Invoke(this, null);

            for (int i = 0; i < drawableActors.Count; i++)
            {
                var item = drawableActors.ElementAt(i).Value;
                if (item.IsPendingKill() || item.bPendingKillDraw)
                {
                    item.bPendingKillDraw = false;
                    drawableActors.RemoveAt(i);
                    i--;
                }
                else if (item.bActive)
                    item.DoDraw();
            }

            spriteBatch.End();

            spriteBatch.Begin();

            if (draw2DMethod != null)
                draw2DMethod.Invoke(this, null);

            foreach (var item in drawableActors)
                if (item.Value.bActive)
                    item.Value.DoDraw2D();

            spriteBatch.End();
        }

        public static T Load<T>(Scene scene = null) where T : Scene
        {
            Scene s = null;
            if (scene == null)
            {
                s = (Scene)Activator.CreateInstance(typeof(T));
                s.DoStart();
            }
            else
                s = scene;

            if (!Target.bServerOnly)
                GWindow.scene = s;
            else
                GServer.scene = s;

            return (T)s;
        }

        public static void Reset()
        {
            Network.Stop();

            if (onDisconnectedMethod != null)
            {
                onDisconnectedMethod.Invoke(Scene.current, null);
                return;
            }

            Scene scene = (Scene)Activator.CreateInstance(Scene.current.GetType());
            Load<Scene>(scene);
            scene.DoStart();
        }

        internal static new bool Check(Behaviour instance)
        {
            if (instance == null)
                return false;
            if (instance.actorInstance != null && instance.actorInstance.bPendingKill)
                return false;
            return true;
        }

        public static T GetCurrent<T>() where T : Scene
        {
            return current as T;
        }
    }
}
