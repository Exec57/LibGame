﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace LibExec
{
    public static class Target
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();

        static string[] args;

        internal static bool bServerOnly { get; private set; }

        public static void SetArgs(string[] args)
        {
            Target.args = args;
        }

        public static void SetWindow<T>() where T : GWindow
        {
            bServerOnly = false;

            CreateInstance(typeof(T));
        }

        public static void SetServer<T>() where T : GServer
        {
            string os = Environment.OSVersion.ToString();

            if (os.Contains("Microsoft"))
                AllocConsole();

            bServerOnly = true;

            CreateInstance(typeof(T));
        }

        static void CreateInstance(Type type)
        {
            dynamic game = Activator.CreateInstance(type);
            using (game)
            {
                game.args = args;
                game.Run();
            }
        }
    }
}
