﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace LibExec
{
    public enum MouseButton
    {
        Left,
        Right
    }

    public static class Input
    {
        static KeyboardState keyboard;
        static KeyboardState oldKeyboard;
        static MouseState mouse;
        static MouseState oldMouse;

        internal static void BeginUpdate()
        {
            keyboard = Keyboard.GetState();
            mouse = Mouse.GetState();
        }

        internal static void EndUpdate()
        {
            oldKeyboard = keyboard;
            oldMouse = mouse;
        }

        public static bool GetKey(Keys key)
        {
            return keyboard.IsKeyDown(key);
        }

        public static bool GetKeyDown(Keys key)
        {
            bool state = keyboard.IsKeyDown(key) && oldKeyboard.IsKeyUp(key);
            return state;
        }

        public static bool GetKeyUp(Keys key)
        {
            bool state = keyboard.IsKeyUp(key) && oldKeyboard.IsKeyDown(key);
            return state;
        }

        public static Vector2 GetMousePosition()
        {
			return new Vector2(mouse.X, mouse.Y);
        }

        public static bool GetMouseButton(MouseButton mouseButton)
        {
            if (mouseButton == MouseButton.Left)
                return mouse.LeftButton == ButtonState.Pressed;
            else if (mouseButton == MouseButton.Right)
                return mouse.RightButton == ButtonState.Pressed;
            return false;
        }

        public static bool GetMouseButtonDown(MouseButton mouseButton)
        {
            bool state = false;
            if (mouseButton == MouseButton.Left)
                state = mouse.LeftButton == ButtonState.Pressed && oldMouse.LeftButton == ButtonState.Released;
            else if (mouseButton == MouseButton.Right)
                state = mouse.RightButton == ButtonState.Pressed && oldMouse.RightButton == ButtonState.Released;
            return state;
        }

        public static bool GetMouseButtonUp(MouseButton mouseButton)
        {
            bool state = false;
            if (mouseButton == MouseButton.Left)
                state = mouse.LeftButton == ButtonState.Released && oldMouse.LeftButton == ButtonState.Pressed;
            else if (mouseButton == MouseButton.Right)
                state = mouse.RightButton == ButtonState.Released && oldMouse.RightButton == ButtonState.Pressed;
            return state;
        }
    }
}
