﻿using LibExec;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGame
{
    public class Projectile : Actor
    {
        Vector2 velocity;

        float speed = 1000;

        public Projectile()
        {
            width = 8;
            height = 8;
            color = Color.Black;

            SetLifeSpan(3);
        }

        void Update()
        {
            Move(velocity * Time.deltaTime * speed);
        }

        public void SetVelocity(Vector2 velocity)
        {
            this.velocity = velocity;
        }
    }
}
