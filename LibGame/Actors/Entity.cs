﻿using LibExec;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGame
{
    public class Entity : Actor
    {
        Vector2 direction;
        float angle = 45;

        public Entity()
        {
            bReplicate = true;
            bReplicateMovement = true;
            bReplicateRotation = true;

            width = 100;
            height = 100;
            origin = new Vector2(width / 2, height / 2);
            color = Color.LightGray;

            SetPosition(new Vector2(300, 300));

            if (HasAuthority())
                Timer.Set(ChangeDirection, this, 2, true, 0);
        }

        void Update()
        {
            if (HasAuthority())
            {
                Move(direction * Time.deltaTime * 50);
                Rotate(angle * Time.deltaTime);
            }
        }

        void ChangeDirection()
        {
            direction = RandomDirection();
            angle *= -1;
        }

        Vector2 RandomDirection()
        {
            Random random = new Random();
            int num = random.Next(0, 4);
            if (num == 0)
                direction = Vector2.UnitX;
            else if (num == 1)
                direction = Vector2.UnitY;
            else if (num == 2)
                direction = Vector2.UnitX * -1;
            else
                direction = Vector2.UnitY * -1;
            return direction;
        }
    }
}
