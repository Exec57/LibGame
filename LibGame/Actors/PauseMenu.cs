﻿using LibExec;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGame
{
    public class PauseMenu : Actor
    {
        SpriteFont font;

        string[] buttons = new string[]
        {
            "Resume",
            "Back to main menu",
            "Quit"
        };

        int selectButton;

        public PauseMenu()
        {
            layer = 2;
        }

        void Start()
        {
            font = Resources.Get<SpriteFont>(Data.Font);
        }

        void Update()
        {
            if (Input.GetKeyDown(Keys.S) || Input.GetKeyDown(Keys.Down))
                selectButton++;
            if (Input.GetKeyDown(Keys.W) || Input.GetKeyDown(Keys.Up))
                selectButton--;
            if (selectButton < 0)
                selectButton = buttons.Length - 1;
            if (selectButton >= buttons.Length)
                selectButton = 0;

            if (Input.GetKeyDown(Keys.Enter))
            {
                if (selectButton == 0)
                {
                    Player.local.SetUpdatable(true);
                    SetActive(false);
                }
                else if (selectButton == 1)
                    Scene.Reset();
                else if (selectButton == 2)
                    GWindow.ExitGame();
            }
        }

        void Draw2D()
        {
            spriteBatch.Draw(Resources.whiteTexture, Screen.rectangle, new Color(50, 50, 50, 200));

            spriteBatch.DrawStringCenter(font, "Pause Menu", new Vector2(Screen.center.X, 50), Color.White);

            for (int i = 0; i < buttons.Length; i++)
            {
                Color buttonColor = Color.White;
                if (selectButton == i)
                    buttonColor = new Color(150, 50, 50);
                spriteBatch.DrawStringCenter(font, buttons[i], new Vector2(Screen.center.X, 200 + i * font.LineSpacing), buttonColor);
            }
        }
    }
}
