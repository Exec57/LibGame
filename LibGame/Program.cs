﻿using LibExec;
using System;

namespace LibGame
{
#if WINDOWS || LINUX || SERVER
    public static class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Target.SetArgs(args);

#if !SERVER
            Target.SetWindow<LibGame>();
#else
            Target.SetServer<LibGameServer>();
#endif
        }
    }
#endif
}