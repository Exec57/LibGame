﻿using LibExec;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Threading;

namespace LibGame
{
    public enum Data
    {
        Font,
        Screen,
        Player
    }

    public class LibGame : GWindow
    {
        void Start()
        {
            Resources.Set<SpriteFont>(Data.Font, "Font");

            Resources.Set<Texture2D>(Data.Screen, "Images/Screen");
            Resources.Set<Texture2D>(Data.Player, "Images/Player");

#if DEBUG
            Scene.Load<SceneGame>();

            if (Network.IsServer())
                Window.Title = "Server";
            else
                Window.Title = "Client";
#else
            Scene.Load<SceneMenu>();
#endif
        }

        void Update()
        {
            if (Input.GetKeyDown(Keys.H) && !Network.IsValid())
            {
                Network.ListenServer(7777);
                Scene.Load<SceneGame>();
            }

            if (Input.GetKeyDown(Keys.C) && !Network.IsValid())
                Network.Connect("127.0.0.1", 7777);
        }
    }
}
